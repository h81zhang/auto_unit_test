import unittest
from unittest import mock
import fibnaci
from fibnaci import multiply
from fibnaci import sum_int
from fibnaci import abs_num
from fibnaci import multiply_five_numver
from fibnaci import get_global
from fibnaci import handle_list


class Test_multiply(unittest.TestCase):
    def test_frd0(self):
        result = multiply(1,2)
        self.assertEqual(2, result)

    def test_frd1(self):
        result = multiply(-1,2)
        self.assertEqual(-2, result)


class Test_sum_int(unittest.TestCase):
    def test_frd0(self):
        result = sum_int(1,2)
        self.assertEqual(3, result)

    def test_frd1(self):
        result = sum_int(2,5)
        self.assertEqual(7, result)


class Test_abs_num(unittest.TestCase):
    def test_frd0(self):
        result = abs_num(-1)
        self.assertEqual(1, result)

    def test_frd1(self):
        result = abs_num(1)
        self.assertEqual(1, result)


class Test_multiply_five_numver(unittest.TestCase):
    def test_frd0(self):
        result = multiply_five_numver(1,2,3,1,1)
        self.assertEqual(6, result)


class Test_get_global(unittest.TestCase):
    def test_frd0(self):
        result = get_global()
        self.assertEqual(5, result)


class Test_handle_list(unittest.TestCase):
    def test_frd0(self):
        result = handle_list([1,2,3,4,5])
        self.assertEqual([3,4,5,6,7], result)


if __name__ == '__main__':
    unittest.main()
